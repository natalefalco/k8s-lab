gcloud compute instances create k8s-demo-master --zone=europe-west3-c \
--machine-type=e2-medium \
--image=ubuntu-2004-focal-v20210603 \
--image-project=ubuntu-os-cloud \
--boot-disk-size=50

gcloud compute instances create k8s-demo-worker --zone=europe-west3-c \
--machine-type=e2-medium \
--image=ubuntu-2004-focal-v20210603 \
--image-project=ubuntu-os-cloud \
--boot-disk-size=50

gcloud compute firewall-rules create nodeports --allow tcp:30000-40000

gcloud compute ssh k8s-demo-master

# in the ssh session

cat <<EOF | sudo tee /etc/modules-load.d/crio.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

# Set up required sysctl params, these persist across reboots.
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sudo sysctl --system

OS=xUbuntu_20.04
VERSION=1.21

sudo apt update
sudo apt install -y apt-transport-https ca-certificates curl

# crio

cat <<EOF | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/ /
EOF

cat <<EOF | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:$VERSION.list
deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/$VERSION/$OS/ /
EOF

# kubadm
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# crio
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/Release.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/libcontainers.gpg add -
curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$VERSION/$OS/Release.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/libcontainers-cri-o.gpg add -

# kubeadm
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

sudo apt-get update
sudo apt-get install -y cri-o cri-o-runc kubelet kubeadm kubectl

sudo systemctl daemon-reload
sudo systemctl enable crio --now

####
####
# only master

sudo kubeadm init 

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

curl https://docs.projectcalico.org/manifests/calico-etcd.yaml -s | kubectl apply -f -

####
####
# only worker
# kubeadm join ....
# if you lost the original 
# kubeadm token create --print-join-command


####
# user owned cluster
civo kubernetes create noel --size g3.k3s.small --nodes 2 --network noel

civo kubernetes create peti --size g3.k3s.small --nodes 2 --network peti

civo kubernetes create marci --size g3.k3s.small --nodes 2 --network marci
