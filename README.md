# k8s Lab

## Setup tools to access cluter

### download kubectl

#### macOS

```bash
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/darwin/amd64/kubectl"
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
sudo chown root: /usr/local/bin/kubectl
```
or 
```bash
brew install kubectl 
```

#### linux

```bash
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

### Setup kubeconfig

create .kube folder to make the config default config

```bash
mkdir ~/.kube/
cat << EOF > ~/.kube/config
here comes the config from the chat
EOF
```

### Test connection and config

```bash
kubectl cluster-info
```

### Setup auto comlete (Optional)

```bash
source <(kubectl completion bash)
```

```zsh
source <(kubectl completion zsh)
```

## Look around what things we have in the cluster

### Cluster brief summary 

Get a brief summary what we have

```bash
kubectl get all --all-namespaces
```

### k8s components

```bash
kubectl -n kube-system get all
```

which component we dont see here?

where is that running?


## Create our own namepsace where we can do stuff

```bash
kubectl create namespace $(whoami)
```

and set our profile to use that namespace

```bash
kubectl config set-context --current --namespace=$(whoami)
```

## Create bare pod with no replication

### Run the pod

There is multiple ways to achieve this with kubectl, create a yaml and apply that, or

```bash
kubectl run nginx --image nginx # and a whole lot of args that you can pass in
#       ^ only pods can "run" the other stuff uses create resourcetype rerourcename syntax
```

### Check if it is runnning or someone messed up

```bash
kubectl get po # we are getting all of the pod the current namespace
kubectl describe pod # we are getting detailed explanation of all the pods in the current namespace
```

### Exec into the pod and kill the main process (Optional)

```bash
kubectl exec -it pod/nginx -- nginx -s quit
```

and now check the status

```
kubectl get pod
```

### Kill the pod 

```bash
kubectl delete pod nginx 
```

## Deployment

### Create deployment 

```bash
kubectl create deploy nginx --image nginx:1.20-alpine --replicas 3
```

### Inspect the deployment

```bash
kubectl describe deployment nginx
```

check the replicasets

```bash
kubectl get replicaset
```

check the pods

```bash
kubectl get pod
```

### Create a new version and pause the rollout

```bash
kubectl set image deploy nginx nginx=nginx:1.21-alpine
sleep 3
kubectl rollout pause deployment nginx
```

now it should have 3-4 pods on this deployment depending when it stopped the rollout and possibly have differet image pods

take a look

```bash
kubectl get pod
```

if you only pods from 1 replicaset they resume the deployment and try it out with a different verion

```bash
kubectl rollout resume deployment nginx
```

to see the rolling update (New, Terminate, New, Terminate)

```bash
kubectl rollout resume deployment nginx
watch -- kubectl get pod
```

### Expose things

until now our apps did not do anyting useful lets expose it to the public

```bash
kubectl expose deployment/nginx --port 80 --target-port 80 --name nginx-service
```

check the service and try to access it

```bash
kubectl get svc

curl -m5 http://<IP from above>
# just in case try the external ip too
curl -m5 http://<cluster external ip>
```

it fails, why? aside of course the the private ip (it would still fail if someone on the same network)

Dramatic Pause

The default expose is only allowing to access from inside the cluster 

now create a debug pod and try it from there

```bash
kubectl run debug --image alpine -- sleep 9d
kubectl exec -it debug -- wget -O- <IP from above>
```

why is the sleep needed?

was it successful?

now expose it for the world and check the service

```bash
kubectl expose deploy/nginx --port 80 --target-port 80 --type NodePort --name nginx-nodeport
kubectl describe svc nginx-nodeport
```

now try to access it, try to get the required infos from svc, node resrouces

```bash
curl http://<node-external-ip>:<nodeport> 
```

now it should succeed 

### Ingress

first take a look what is ingress

```bash
kubectl get all -n kube-system -l app=traefik 
```

We use traefik controller because I wanted to try it out, the default is nginx

The main thing is that there is a controller in kube-system namespace that listens in All namespace for ingress resources

Let's crate some ingress
note that the namespace is different to the controllers (the controller sees it in each ns and acts upon the changes)

```bash
cat << EOF | kubectl apply -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
 name: nginx
 namespace: $(whoami)
 annotations:
   kubernetes.io/ingress.class: traefik
   traefik.frontend.rule.type: PathPrefixStrip
spec:
 rules:
 - http:
     paths:
     - path: /app
       pathType: Prefix # Exact
       backend:
         service:                                                                                                                                                                                                           
           name: nginx-service                                                                                                                                                                                                 
           port:                                                                                                                                                                                                            
             number: 80        
EOF
```

okay try to access it

```bash
kubectl get ingress
curl http://<ip from above>/app
```

A little explanation why we have port 80
if you check out the 

```bash
kubectl get svc -n kube-system
```

you see that there is a load balancer resource called traefik which has a weird ports description,
that means it exposes the port on each node and sets up a loadbalancer to forward the port for us
so if you would just create an ingress controller and resource you need to expose that either with loadbalancer or external port

## Metrics, Logs and everything nice

for sure you probably dont need it but it is good to measure what you need

gettin' the metrics, although it aint fast, enough for profiling

```bash
kubectl top pod
kubectl top node
```

also create some load example:
```bash
kubectl exec -it pod/debug -- sh -c "for i in \$(seq 50); do wget -O /dev/null -o /dev/null http://nginx-service &  done; wait"
```

slowly the cpu will rump up a bit on both nginx and the debug

gettin' the logs 

```bash
kubectl logs deploy/nginx 
# it will select one from the deployment and how that
```

this works for deployments, statefulsets, daemonsets, pods

## Configmap and secret

for now we will play with config maps only

```bash
cat << EOF | kubectl apply -f -
apiVersion: v1
data:
  index.html: "<!DOCTYPE html>\n<html>\n<head>\n  <meta charset=\"utf-8\">\n  <title>Hello
    K8s</title>\n</head>\n<body>\n<video id=\"vid\" muted controls autoplay>\n  <source
    src=\"https://s3.eu-north-1.amazonaws.com/public.noelsr.dev/k8s.mp4\" type=\"video/mp4\">\n
    \ <source src=\"https://s3.eu-north-1.amazonaws.com/public.noelsr.dev/k8s.webm\"
    type=\"video/webm\">\n  \n</video>
    \n\n</body>\n</html>\n"
kind: ConfigMap
metadata:
  creationTimestamp: null
  name: webpage
EOF
```

or create by yourself

```bash
kubectl create configmap webpage --from-file index.html
```

patch or edit or apply the nginx deploy to have the volume said here

```bash
cat << EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: nginx
  name: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx:1.20-alpine
        name: nginx
        resources: {}
        volumeMounts:
        - mountPath: /usr/share/nginx/html/index.html
          name: nginx-conf
          subPath: index.html
      volumes:
      - name: nginx-conf
        configMap:
          name: webpage

status: {}
EOF
```

you will want you can try pause the rollout, by that you will see that some of the traffic goes to the old and some to the new
if you suck try to increase the replicas then remove the volume and pause

```bash
kubectl scale deployment nginx --replicas 5
```

lets try the node failure:
ping me that you want to do this, I will add a node to the cluster
create a loop checking the reponse of the service
have the pods scheduled on multiple nodes try delete the pod

```bash
kubectl get pod -o wide
```

example to check systems status (works on the debug container too)

```bash
while true; do wget -O /dev/null http://cluster-ip/app 2>/dev/null 1>/dev/null  || echo err ; done
```

## Extra

install helm

```bash
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

```bash
brew install helm
```


install logging monitoring stack

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm upgrade --install loki grafana/loki-stack  --set grafana.enabled=true,prometheus.enabled=true,prometheus.alertmanager.persistentVolume.enabled=false,prometheus.server.persistentVolume.enabled=false
```

get admin secret
```bash
kubectl get secret --namespace <loki namespace> loki-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

create a proxy and connect to localhost:3000 with browser

```bash
kubectl port-forward --namespace <loki namespace> service/loki-grafana 3000:80
```

browse the loki for logs there will be only default metrics


just for the note 

on the left select explore

in the query put this 
{job="noel/nginx"} != "wget"

https://grafana.com/docs/loki/latest/logql/

grafana

```
sum(rate(container_cpu_usage_seconds_total{namespace="noel", pod!=""}[5m])) by (pod)

sum(container_memory_usage_bytes{namespace="noel", pod!=""}) by (pod)
```
